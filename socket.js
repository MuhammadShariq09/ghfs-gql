const server = require('http').Server();
const io = require('socket.io')(server);
const Redis = require('ioredis');
const redis = new Redis();
require('dotenv').config()


const laravePrefix = 'go_hard_finish_strong_database_private';

redis.psubscribe(`${laravePrefix}-user-channel`);

redis.on('pmessage', function(channel, s, message){

    message = JSON.parse(message);

    console.log(`${channel.replace('go_hard_finish_strong_database_','')}:${message.event}`);

    // console.log(message, channel.replace('go_hard_finish_strong_database_',''));

    io.emit(`${channel.replace('go_hard_finish_strong_database_','')}:${message.event}`, message);
});

const port = process.env.MIX_SOCKET_PORT;
// const port = 9999;

server.listen(port, function(){
    console.log(`server is listening on port ${port}`);
});
