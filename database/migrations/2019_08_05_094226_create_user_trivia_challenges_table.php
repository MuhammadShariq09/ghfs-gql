<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTriviaChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_trivia_challenges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('challenge_id');
            $table->unsignedBigInteger('user_id');
            $table->text('challenge_data');
            $table->unsignedInteger('total_questions');
            $table->unsignedInteger('attempted_questions');
            $table->unsignedInteger('correct_questions');
            $table->unsignedInteger('wrong_questions');
            $table->unsignedInteger('earned_points');
            $table->unsignedInteger('duration');
            $table->boolean('paid')->default(false);
            $table->timestamp('ended_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_trivia_challenges');
    }
}
