<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->unsignedSmallInteger('sets')->default(1);
            $table->unsignedSmallInteger('repeat')->default(1);
            $table->text('description')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('challenge_id');
            $table->timestamps();
            $table->foreign('challenge_id')->on('challenges')->references('id');
            $table->foreign('category_id')->on('categories')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercises');
    }
}
