<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengeRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenge_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sender_id');
            $table->unsignedBigInteger('recipient_id');
            $table->unsignedBigInteger('challenge_id');
            $table->tinyInteger('status')->default(0);
            $table->string('sender_result')->nullable();
            $table->string('recipient_result')->nullable();
            $table->boolean('recipient_submitted')->default(false);
            $table->boolean('sender_submitted')->default(false);
            $table->dateTime('voting_expired_at')->nullable();
            $table->text('challenge_data')->nullable();
            $table->text('sender_points')->nullable();
            $table->text('recipient_points')->nullable();
            $table->unsignedInteger('leverage_points')->default(0);
            $table->unsignedInteger('earned_leverage_points')->default(0);
            $table->unsignedBigInteger('leverage_user_id')->nullable();
            $table->string('duration')->nullable();
            $table->timestamp('challenge_accepted_at')->nullable();
            $table->timestamp('time_to_complete')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenge_requests');
    }
}
