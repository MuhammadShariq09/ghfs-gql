<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriviaChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trivia_challenges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('time')->default(0);
            $table->string('type')->default('Free');
            $table->string('amount')->default('0');
            $table->string('points')->default(0);
            $table->string('image')->default(0);
            $table->string('prize')->nullable();
            $table->string('prize_value')->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('completed')->default(false);
            $table->unsignedBigInteger('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trivia_challenges');
    }
}
