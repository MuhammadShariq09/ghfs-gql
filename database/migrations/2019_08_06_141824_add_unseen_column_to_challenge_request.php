<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnseenColumnToChallengeRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('challenge_requests', function (Blueprint $table) {
            $table->boolean('sender_seen')->default(false);
            $table->boolean('recipient_seen')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('challenge_requests', function (Blueprint $table) {
            $table->dropColumn('sender_seen');
            $table->dropColumn('recipient_seen');
        });
    }
}
