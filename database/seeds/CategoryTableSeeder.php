<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = collect(['Upper Body', 'Lower Body', 'Core', 'Full Body', 'Back Body', 'Front Abs']);

        $categories->each(function($category){
            factory(\App\Models\Category::class)->create([
                'created_by' => rand(1, 3),
                'title' => $category,
                'slug' => \Illuminate\Support\Str::slug($category)
            ]);
        });

    }
}
