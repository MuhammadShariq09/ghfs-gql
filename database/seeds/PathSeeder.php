<?php

use Illuminate\Database\Seeder;

class PathSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paths = ['EAST', 'WEST', 'NORTH', 'SOUTH'];
        $level = 1;

        foreach($paths as $path)
        {
            for ($i = 0; $i < 10; $i++)
            {
                factory(\App\Models\Path::class)->create(['path' => $path, 'level' => $level, 'points' => $level*2500]);
                $level++;
            }
        }
    }
}
