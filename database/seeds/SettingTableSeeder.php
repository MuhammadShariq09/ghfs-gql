<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            "leverage_points" => 20,
            "challenge_points" => 30,
            "challenge_accept_points" => 30,
            "video_posted_points" => 50,
            "max_no_challenge_accepted_in_72_hr" =>  2,
            "time_to_accept_a_challenge" => 10,
            "time_to_complete_a_challenge" => 24,
            "min_exercise_for_a_challenge" => 10,
            "time_to_select_another_challenge" => 60,
            "time_to_approve_or_deny_challenge" => 60,
            "voting_time_for_a_challenge" => 60,
            "time_format" => "Hours",
            "allowed_requests_monthly" => 60,
            "allowed_votes_monthly" => 60,
            "acceptable_challenge_monthly" => 10,
            "no_of_days_to_reset_user_settings" => 30,
            "points_earned_per_vote" => 10,
            "reset_days" => 30,
            "minimum_users_to_show_trivia_challenge" => 500,
        ];

        setting()->setExtraColumns(['guard' => 'admin']);

        setting($settings)->save();
    }
}
