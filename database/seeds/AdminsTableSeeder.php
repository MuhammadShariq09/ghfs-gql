<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Administrator\Admin::class)->create(['email' => 'charlestsmith888@gmail.com', 'name' => 'Charlest Smith', 'password' => bcrypt('Admin123')]);
        factory(\App\Models\Administrator\Admin::class, 2)->create();
    }
}
