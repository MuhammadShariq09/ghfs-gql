<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*$faker = Faker\Factory::create();
        \App\User::all()->each(function($user) use($faker){
//            $user->image = 'images/challenges/' . $faker->image( storage_path('app/public/images/profile/'),250, 250, 'fashion', false);
//            $user->image = str_replace('images/challenges/','images/profile/', $user->image);
            $user->save();
        });*/

        $this->call(UsersTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(StaffTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(ChallengesTableSeeder::class);
        $this->call(SettingTableSeeder::class);
        $this->call(PathSeeder::class);

        Artisan::call('passport:install');
    }
}
