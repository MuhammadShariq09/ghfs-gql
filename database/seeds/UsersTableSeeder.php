<?php

use App\Models\ChallengeRequest;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boolean = [true, false];


        // create Saad Asad Account for testing
        $user = factory(\App\User::class)->create(['email' => 'saad@mailinator.com', 'name' => 'Saad Asad']);

        $user->addState();

        // create Stat for Saad Asad Account
        factory(\App\Models\State::class)->create(['user_id' => $user->id]);

        // create friends for Saad
        $friends = \App\User::inRandomOrder()->where('id', '!=', $user->id)->limit(10)->get();

        $friends->each(function($friend) use($user, $boolean){
            $user->befriend($friend);

            if($boolean[rand(0, 1)])
                $friend->acceptFriendRequest($user);
        });

        $users = factory(\App\User::class, 49)->create();

        $users->each(function($user) use($boolean){

            factory(\App\Models\State::class)->create(['user_id' => $user->id]);

            $friends = \App\User::inRandomOrder()->where('id', '!=', $user->id)->limit(10)->get();

            $friends->each(function($friend) use($user, $boolean){
                $user->befriend($friend);

                if($boolean[rand(0, 1)])
                    $friend->acceptFriendRequest($user);
            });


        });

        $challengeStatus = [\App\Core\Status::PENDING, \App\Core\Status::ACCEPTED, \App\Core\Status::DENIED];

        $users->each(function($user) use($boolean, $challengeStatus){

            $friends = $user->getFriends();

            $friends->each(function($friend) use($user, $challengeStatus){
                $user->challenges()->save(new ChallengeRequest([
                    'recipient_id' => $friend->id,
                    'challenge_id' => rand(1, 50),
                    'status' => $challengeStatus[rand(0, count($challengeStatus)-1)]
                ]));
            });

        });
    }
}
