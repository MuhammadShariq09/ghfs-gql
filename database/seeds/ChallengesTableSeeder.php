<?php

use Illuminate\Database\Seeder;

class ChallengesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Challenge::class, 50)->create(['created_by' => rand(1, 3)])
            ->each(function ($challange){
                factory(\App\Models\Exercise::class, rand(5, 10))->create(['challenge_id' => $challange->id, 'category_id' => rand(1, 6)]);
            });

        $requests = \App\Models\ChallengeRequest::with('challenge.exercises')->get();

        $requests->each(function($req){
            $req -> challenge_data = $req->challenge;
            $req->save();
        });

    }
}
