<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Path;
use Faker\Generator as Faker;

$factory->define(Path::class, function (Faker $faker) {
    return [
        'path' => $faker->word,
        'level' => $faker->numberBetween(1, 40),
        'points' => $faker->numberBetween(25000, 100000),
    ];
});
