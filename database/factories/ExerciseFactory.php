<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Exercise;
use Faker\Generator as Faker;

$factory->define(Exercise::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'sets' => $faker->numberBetween(5, 50),
        'repeat' => $faker->numberBetween(1, 10),
        'description' => $faker->paragraph,
        'category_id' => function(){
            return factory(\App\Models\Category::class)->create()->id;
        },
        'image' => 'images/exercises/' . $faker->image( storage_path('app/public/images/exercises/'),400, 400, 'nightlife', false),
        'challenge_id' => function(){
            return factory(\App\Models\Challenge::class)->create()->id;
        }
    ];
});


$possibleCategories = array(
    'abstract', 'animals', 'business', 'cats', 'city', 'food', 'nightlife',
    'fashion', 'people', 'nature', 'sports', 'technics', 'transport'
);

