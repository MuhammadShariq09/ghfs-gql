<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    $title = $faker->sentence;
    return [
        'title' => $title,
        'slug' => \Illuminate\Support\Str::slug($title),
        'description' => $faker->paragraph,
        'created_by' => function(){
            return factory(\App\Models\Administrator\Admin::class)->create()->id;
        }
    ];
});
