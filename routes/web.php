<?php

use \Illuminate\Support\Facades\Route;
use \Illuminate\Support\Facades\Auth;

Route::redirect('/', '/admin');

Route::view('privacy-policy', 'privacy-policy')->name('privacy-policy');

/*Route::get('/', function (Illuminate\Http\Request $request) {


//    1) Publish event with redis
    $data = [
            'user_id' => '1',
            'user_name' => 'arifiqbal'
        ];

    $user = User::first();

    auth()->login($user);

    event(new UserRegistered($user));

//    Redis::publish('user-channel', json_encode($data));

    return 'done';
//    2) Node + Redis subscribe to the event
//    3) Use socket to emit to all client

});*/

Auth::routes();

Route::get('/{home?}', 'HomeController@index')->where('home', '(!admin|!staff).*');
