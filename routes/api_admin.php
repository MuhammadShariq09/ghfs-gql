<?php

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Route;


Route::middleware('auth:api_admin')->group(function(){
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::put('/user', 'AuthController@profilePut');
});

// public routes
Route::post('/login', 'AuthController@login')->name('admin.login.api');
Route::post('/register', 'AuthController@register')->name('admin.register.api');

Route::group(['prefix' => 'password'], function(){
    Route::post('create', 'PasswordResetController@create');
    Route::get('toke/validate/{token}', 'PasswordResetController@validateToken');
    Route::post('reset', 'PasswordResetController@reset');
});

// private routes only for normal users
Route::middleware('auth:api_admin')->group(function () {

    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::get('/settings', 'SettingsController@index')->name('settings');
    Route::get('/settings/{key}', 'SettingsController@show')->name('settings.show');
    Route::post('/settings', 'SettingsController@store')->name('settings.store');
    Route::get('/users', 'UsersController@index')->name('users.index');
    Route::get('/users/profile', 'UsersController@profile')->name('users.profile');
    Route::get('/users/{user}', 'UsersController@show')->name('users.show');
    Route::put('/users/{user}', 'UsersController@update')->name('users.update');
    Route::put('/users/{user}/status', 'UsersController@statusPut')->name('users.status');
    Route::post('/users', 'UsersController@store')->name('users.store');
    Route::get('/users/{user}/friends', 'UsersController@friends')->name('users.friends');

    Route::apiResource('challenges', 'ChallengeController');
    Route::delete('challenges/{challenge}/exercises/{exercise}', 'ChallengeController@deleteExercise');
    Route::apiResource('categories', 'CategoriesController');
    Route::apiResource('fit-to-paths', 'PathController');
    Route::get('active-challenges', 'ChallengeController@activeChallenges');

    Route::get('pooling-requests', 'PoolingRequestsController@index');
    Route::get('pooling-requests/{challengeRequest}', 'PoolingRequestsController@show');
    Route::post('pooling-requests/declare-result', 'PoolingRequestsController@declareResult');

    Route::post('/check', 'UniqueValidationController@check');

    Route::get('/feedback', 'FeedbackController@index');

    Route::get('/notifications', 'NotificationsController@index');
    Route::patch('/notifications/{notification}/read', 'NotificationsController@readNotification');

    Route::prefix('trivia')->group(function(){
        Route::apiResource('challenges', 'TriviaChallengeController');
    });

});
