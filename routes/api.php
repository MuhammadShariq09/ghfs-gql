<?php

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// public routes
Route::post('/login', 'AuthController@login')->name('login.api');
Route::post('/register', 'AuthController@register')->name('register.api');

Route::group(['prefix' => 'password'], function(){
    Route::post('create', 'PasswordResetController@create');
    Route::get('toke/validate/{token}', 'PasswordResetController@validateToken');
    Route::post('reset', 'PasswordResetController@reset');
});

Route::get('questions/start', 'TriviaChallengeController@start');

// private routes only for normal users
Route::middleware(['auth:api'])->group(function () {

    Route::get('/logout', 'AuthController@logout')->name('logout');
    Route::apiResource('/challenges', 'ChallengesController');


    Route::prefix('user-challenge')->group(function(){
        Route::get('{challengeRequest}/show', 'ChallengeRequestController@show');
        Route::get('{challengeRequest}/votes', 'ChallengeRequestController@votings');
        Route::get('requests', 'ChallengeRequestController@currentUserChallengeRequests');
        Route::post('requests', 'ChallengeRequestController@request');
        Route::post('accept', 'ChallengeRequestController@accept');
        Route::post('reject', 'ChallengeRequestController@reject');
        Route::post('declare-result', 'ChallengeRequestController@declareResult');
        Route::get('invitations', 'ChallengeRequestController@invitations');
        Route::post('submit', 'ChallengeRequestController@submitVideo');
        Route::post('submit-challenge', 'ChallengeRequestController@submitChallenge');
        Route::post('leverage-points', 'ChallengeRequestController@leveragePointsPost');

        Route::get('votings', 'VotingController@index');
        Route::post('votes', 'VotingController@store');
    });

    Route::prefix('friendships')->group(function(){
        Route::get('requests', 'FriendshipController@myFriendRequests');
        Route::post('requests', 'FriendshipController@request');
        Route::post('accept', 'FriendshipController@accept');
        Route::post('reject', 'FriendshipController@reject');
        Route::post('cancel', 'FriendshipController@cancel');
        Route::post('unfriend', 'FriendshipController@unfriend');
    });

    Route::prefix('trivia')->group(function(){
        Route::get('questions', 'TriviaChallengeController@index');
        Route::post('questions/start', 'TriviaChallengeController@start');
    });

    Route::get('/users/profile', 'Administrator\UsersController@profile')->name('users.profile');
    Route::get('/users/{user}/friends', 'UsersController@friends')->name('users.friends');
    Route::get('/users', 'UsersController@index')->name('users.index');
    Route::get('/users/profile', 'UsersController@profile')->name('users.profile');
    Route::get('/users/logs', 'UsersController@logs')->name('users.logs');
    Route::get('/users/pay-logs', 'UsersController@payLogs')->name('users.pay.logs');
    Route::get('/users/pay-logs/{transaction_id}', 'UsersController@payLogsShow')->name('users.pay.logs.detail');
    Route::put('/users/profile', 'UsersController@profilePut')->name('users.profile.put');
    Route::get('/users/{user}', 'UsersController@show')->name('users.show');
    Route::put('/users/{user}/status', 'UsersController@statusPut')->name('users.status');
    Route::post('/users', 'UsersController@store')->name('users.store');

    Route::post('/feedback', 'FeedbackController@store')->name('feedback.store');

    Route::prefix('trivia')->group(function(){
        Route::post('challenges/answer/check', 'TriviaChallengeController@checkAnswer');
        Route::post('challenges/finish', 'TriviaChallengeController@finish');
        Route::get('challenges/running', 'TriviaChallengeController@running');
        Route::apiResource('challenges', 'TriviaChallengeController');
    });
});

Route::post('/users/image', 'Administrator\UsersController@store')->name('users.store');
Route::delete('/users/image', 'Administrator\UsersController@destroy')->name('users.destroy');

