<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'admin/dashboard');

Route::get('logout', 'Auth\LoginController@logout');

Auth::routes();

Route::middleware('auth:admin')->get('/{admin?}', 'HomeController@index')->where('admin', '.*')->name('home');

//Route::get('/{any?}', function(){
//    dd('we here in admin route');
//})->where('any', '.*')->name('home');
