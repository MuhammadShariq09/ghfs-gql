<?php

namespace App\Core\Filters;

use App\Models\ChallengeRequest;

class ChallengesFilters extends Filters {

    protected $filters = ['level', 'path', 'status', 'search_term', 'recipient'];

    public function level($level)
    {
        $this->builder->where('level', "Level {$level}");
    }

    public function path($path)
    {
        $this->builder->where('path', $path);
    }

    public function status($status)
    {
        if($status == 1)
            $this->builder->whereIn('status', [1, 4]);
        else
            $this->builder->whereStatus($status);
    }

    public function search_term($term)
    {
        $this->builder->where(function($q)use($term){
            $q->where('title', "LIKE", '%' . $term . '%')
//            ->orWhere('description', "LIKE", '%' . $term . '%')
            ->orWhere('reward_points', $term);
        });
    }

    public function recipient($recipient_id)
    {
        $challenges = ChallengeRequest::query()->where(function($q) use($recipient_id){
            $q->where('sender_id', auth()->id())
                ->where('recipient_id', $recipient_id);
        })->orWhere(function($q) use($recipient_id){
            $q->where('recipient_id', auth()->id())
                ->where('sender_id', $recipient_id);
        })
            // ->dump()
            ->pluck('challenge_id');

        $this->builder->whereNotIn('id', $challenges);
    }
}
