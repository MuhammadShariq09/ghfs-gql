<?php

namespace App\Core\Filters;

class UserFilters extends Filters {

    protected $filters = ['name', 'search_term', 'status'];

    public function name($name)
    {
        $this->builder->where('id', '!=', auth()->id());
        $this->builder->where("name", 'LIKE', "%{$name}%");
    }

    public function status($status)
    {
        $this->builder->whereStatus($status);
    }

    public function search_term($term)
    {
        $this->builder->where(function($q)use($term){
            $q->where('name', "LIKE", '%' . $term . '%')
//            ->orWhere('description', "LIKE", '%' . $term . '%')
                ->orWhere('email', "LIKE", '%' . $term . '%')
                ->orWhere('contact', "LIKE", '%' . $term . '%');
        });
    }

}
