<?php

namespace App\Core\Filters;

use App\Core\Status;

class PoolingFilters extends Filters {

    protected $filters = ['history', 'search_term'];

    public function history($history)
    {
        if($history)
            $this->builder->whereNotNull('voting_expired_at')->where('status', '!=', Status::DISPUTE);
        else
            $this->builder->whereStatus(Status::DISPUTE);
    }

    public function search_term($term)
    {
        $this->builder->where(function($q)use($term){
            $q->orWhereHas('challenge', function($q) use($term){
                $q->where('title', "LIKE", '%' . $term . '%');
            })->orWhereHas('sender', function ($q) use($term){
                $q->where('name', "LIKE", '%' . $term . '%');
            })->orWhereHas('recipient', function ($q) use($term){
                $q->where('name', "LIKE", '%' . $term . '%');
            });

//            $q->where('name', "LIKE", '%' . $term . '%')
//            ->orWhere('description', "LIKE", '%' . $term . '%')
//                ->orWhere('email', "LIKE", '%' . $term . '%')
//                ->orWhere('contact', "LIKE", '%' . $term . '%');
        });
    }

}
