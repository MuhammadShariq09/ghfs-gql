<?php

namespace App;

use App\Core\Status;
use App\Core\Traits\Challengable;
use App\Core\Traits\Friendable;
use App\Core\Traits\ImageableFill;
use App\Core\Traits\Loggable;
use App\Events\ChallengeRequestSent;
use App\Models\ChallengeRequest;
use App\Models\CreditCard;
use App\Models\State;
use App\Models\Transaction;
use App\Models\TriviaChallenge;
use App\Models\UserTriviaChallenge;
use App\Models\Voting;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Cashier;
use Laravel\Cashier\Exceptions\InvalidStripeCustomer;
use Laravel\Cashier\PaymentMethod;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, Friendable, Loggable, Challengable, ImageableFill, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'address', 'status', 'country', 'city', 'address_state', 'postal_code', 'contact', 'image', 'device_id'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'status' => 'boolean',
    ];

    protected $imageFolder = 'profile';

    public function state()
    {
        return $this->hasOne(State::class);
    }

    public function addState()
    {
        $this->state()->save(new State([
            'level' => "1",
            'path' => "EAST",
            'global_path' => "1",
            'global_level' => 0,
            'points' => 0,
            'initial_requests' => setting('allowed_requests_monthly', 0),
            'challenge_votes' => setting('allowed_requests_monthly', 0),
            'accept_requests' => setting('allowed_requests_monthly', 0),
            'remaining_days' => 30,
        ]));
        $this->load('state');
    }

    public function canSendChallengeRequest() {
        return $this->state->initial_requests > 0;
    }

    public function canAcceptChallengeRequest() { return $this->state->accept_requests > 0; }

    public function feedbacks()
    {
        return $this->hasMany(\App\Models\Feedback::class);
    }

    public function challenges()
    {
        return $this->hasMany(\App\Models\ChallengeRequest::class, 'sender_id');
    }

    public function votings()
    {
        return $this->hasMany(\App\Models\Voting::class, 'voter_id');
    }

    public function addVote(ChallengeRequest $challenge, $userID)
    {
        $vote = new Voting(['challenge_id' => $challenge->id, 'vote_for' => $userID]);
        $this->votings()->save($vote);
        return $this;
    }

    public function getChallenges($filter=null)
    {
        return $this->challenges()->filter($filter)->orWhere('recipient_id', $this->id)
            ->with(['sender.state', 'recipient.state']);
    }

    public function getChallengeVotingRequestCount()
    {
        $ids = auth()->user()->getFriendsIds();

        $voting_request_count = ChallengeRequest::whereStatus(Status::IN_VOTING)
            ->where(function($q) use($ids){
                $q->orWhereIn('sender_id', $ids);
                $q->orWhereIn('recipient_id', $ids);
            })
            ->where('sender_id', '!=', auth()->id())
            ->where('recipient_id', '!=', auth()->id())
            ->count();

        $this->voting_request_count = $voting_request_count;

        return $voting_request_count;
    }

    public function setBadgeCount()
    {
        $this->useen_pending_challenges_count = $this->getUnseenCountByStatus(Status::PENDING);
        $this->unseen_accepted_challenges_count = $this->getUnseenCountByStatus(Status::ACCEPTED);
        $this->unseen_voting_challenges_count = $this->getUnseenCountByStatus(Status::IN_VOTING);
        $this->unseen_completed_challenges_count = $this->getUnseenCountByStatus(Status::COMPLETED);
        $this->unseen_submitted_challenges_count = $this->getUnseenCountByStatus(Status::SUBMITTED);
    }

    protected function getUnseenCountByStatus($status)
    {
        // \DB::listen(function($d){ dd($d); });
        return ChallengeRequest::query()->where('status', $status)->where(function($builder){
            $builder->orWhere(function($builder){
                $builder->where('sender_id', $this->id)
                    ->where('sender_seen', false);
            })->orWhere(function($builder){
                $builder->where('recipient_id', $this->id)
                    ->where('recipient_seen', false);
            });
        })->count();
    }

    public function getChallengesCount()
    {
        $this->challenge_request_count      = ChallengeRequest::query()->where('status', Status::PENDING)->where('recipient_id', $this->id)->count();
        $this->challenge_initiated_count    = ChallengeRequest::query()->where('sender_id', $this->id)->count();
        $this->challenge_accepted_count     = ChallengeRequest::whereIn('status', [Status::ACCEPTED, Status::SUBMITTED])->where(function($builder){
            $builder->orWhere(function($builder){
                $builder->orWhere('sender_id', $this->id)
                    ->orWhere('recipient_id', $this->id);
            })->where(function($builder){
                $builder->orWhere('recipient_id', $this->id)
                    ->orWhere('sender_id', $this->id);
            });
        })->count();
        $this->challenge_completed_count    = $this->getCountByStatus(Status::COMPLETED);
        $this->challenge_verification_count = $this->getCountByStatus(Status::IN_VOTING);
    }

    protected function getCountByStatus($status)
    {
        // \DB::listen(function($d){ dd($d); });
        $query = ChallengeRequest::query();

        if(is_array($status))
            $query->whereIn('status', $status);
        else{
            $query->where('status', $status);
        }

        return $query->where(function($builder){
            $builder->orWhere(function($builder){
                $builder->where('sender_id', $this->id)
                    ->where('sender_seen', false);
            })->orWhere(function($builder){
                $builder->where('recipient_id', $this->id)
                    ->where('recipient_seen', false);
            });
        })->count();
    }

    public function sendChallengeRequest($friend, $challenge)
    {
        $challenge->load('exercises');
        $this->challenges()->save(new ChallengeRequest([
            'recipient_id'      => $friend->id,
            'challenge_id'      => $challenge->id,
            'challenge_data'    => $challenge,
            'duration'          => setting('time_to_complete_a_challenge'),
            'leverage_points'   => setting('leverage_points'),
        ]));

        $challengeRequest = $this->challenges()->latest()->first();

        event(new ChallengeRequestSent($challengeRequest));

        return $challengeRequest;
    }

    public function incrementBonusPoints()
    {
        $bonus_applicable_points = $this->state->ftfp_points - $this->state->ftfp_bonus_exclusive_points;

        $multiplier = floor($bonus_applicable_points / setting('bonus_points_interval', 1));

        if($multiplier < 1)
            return false;

        $points = $multiplier * setting('ftfp_bonus_points', 0);

        $this->state->increment( 'points', $points );

        $this->state->increment( 'ftfp_bonus_exclusive_points', $points );

        return $this;
    }

    public function triviaChallenges()
    {
        return $this->belongsToMany(TriviaChallenge::class, UserTriviaChallenge::class, 'user_id', 'challenge_id')
            ->withPivot('challenge_data', 'total_questions', 'attempted_questions', 'correct_questions', 'wrong_questions', 'earned_points', 'duration', 'paid', 'ended_at', 'completed')
            ->withTimestamps();
    }

    public function alreadyAttempted($challenge){
        return $this->triviaChallenges()->whereRaw('`user_trivia_challenges`.`challenge_id` = ' . $challenge->id)->count() > 0;
    }

    /**
     * Add a payment method to the customer.
     *
     * @param  \Stripe\PaymentMethod|string  $paymentMethod
     * @return \Laravel\Cashier\PaymentMethod
     */
    public function addPaymentMethod($card_details)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $paymentMethod = \Stripe\PaymentMethod::create(['type' => 'card', 'card' => $card_details]);

        $this->assertCustomerExists();

        $this->addCards($card_details, $paymentMethod->id);

        $stripePaymentMethod = $this->resolveStripePaymentMethod($paymentMethod);

        if ($stripePaymentMethod->customer !== $this->stripe_id) {
            $stripePaymentMethod = $stripePaymentMethod->attach(
                ['customer' => $this->stripe_id], Cashier::stripeOptions()
            );
        }

        return new PaymentMethod($this, $stripePaymentMethod);
    }


    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'transactor');
    }

    public function addCards($card_details, $stripe_pm_id = null)
    {

        foreach ($card_details as $key => $value)
            $card_details[$key] = encrypt($value);

        $card_details = array_merge($card_details, ['stripe_pm_id' => $stripe_pm_id, 'default' => 1]);

        $card = new CreditCard($card_details);

        $this->cards()->whereDefault(true)->update(['default' => false]);

        $this->cards()->save($card);
    }

    public function cards()
    {
        return $this->hasMany(CreditCard::class, 'user_id');
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

}
