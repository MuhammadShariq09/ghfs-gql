<?php

namespace App\GraphQL\Queries;

use App\Core\Filters\UserFilters;
use App\GraphQL\Exceptions\GQLException;
use App\User;
use GraphQL\Type\Definition\ResolveInfo;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class Friends
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
//        return auth()->user()->;

        $requests = request();
        $requests->merge($args);
        $filters = new UserFilters($requests);;
        /** @var User $user */
        $user = auth()->user();

        if(isset($args['userId'])) $user = User::find($args['userId']);

        if(!$user)
            throw new GQLException("Invalid User ID", $args['userId'] . ' is not a valid user id');

        if(isset($args['page']))
        {
            Paginator::currentPageResolver(function() use($args){
                return $args['page'];
            });
        }

        $friends = $user->getFriendsWithFilters($filters, $args['first']);

        $userIds = $friends->pluck('id');

        $friendsStatus = Friendship::query()->where(function($q) use($userIds){
            $q->whereIn('sender_id', $userIds)
                ->where('recipient_id', auth()->id());
        })->orWhere(function($q) use($userIds){
            $q->whereIn('recipient_id', $userIds)
                ->where('sender_id', auth()->id());
        })->get();


        $friends->each(function(&$user, $index) use($friendsStatus){
            $f = $friendsStatus->filter(function($friendShip) use($user){
                return ($friendShip->sender_id == $user->id && $friendShip->recipient_id == auth()->id())
                    ||
                    $friendShip->sender_id == auth()->id() && $friendShip->recipient_id == $user->id;
            })->first();

            $user->friend_status = $f;
        });

        return $friends->getIterator();
    }
}
