<?php

namespace App\GraphQL\Queries;

use App\Core\Filters\ChallengesFilters;
use App\Core\Status;
use App\GraphQL\Exceptions\GQLException;
use App\Http\Resources\ChallengeResource;
use App\Http\Resources\Invitation;
use App\Models\Challenge;
use App\Models\ChallengeRequest;
use App\User;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class FTFPChallenges
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function getByLevel($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $filters = new ChallengesFilters(request()->merge($args));

        return Challenge::filter($filters)->latest()->with('exercises')->get();
    }

    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param mixed[] $args The arguments that were passed into the field.
     * @param \Nuwave\Lighthouse\Support\Contracts\GraphQLContext $context Arbitrary data that is shared between all fields of a single query.
     * @param \GraphQL\Type\Definition\ResolveInfo $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     * @throws GQLException
     */
    public function pending_challenge_request($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        /** @var User $user */
        $user = auth()->user();

        if(isset($args['page']))
        {
            Paginator::currentPageResolver(function() use($args){
                    return $args['page'];
            });
        }
        return $challengeRequest = ChallengeRequest::with('sender')->whereRecipientId($user->id)->whereStatus(Status::PENDING)->paginate($args['first'])->toArray();
    }
}
