<?php

namespace App\GraphQL\Mutations;

use App\GraphQL\Exceptions\GQLException;
use App\Http\Controllers\Api\ApiBaseController;
use App\User;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class Friendship extends ApiBaseController
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function send_request($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        auth()->user()->befriend(User::find($args['recipient_id']));

        $d = \Hootlex\Friendships\Models\Friendship::latest()->first();

        return $d;
    }

    public function accept_request($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $sender = User::find($args['sender_id']);

        /** @var User $user */
        $user = auth()->user();

        $request = \Hootlex\Friendships\Models\Friendship::where(['sender_id' => $args['sender_id'], 'recipient_id' => $user->id])->first();

        if(!$request)
            throw new GQLException(('errors.unauthenticated'), 'Not found!');

        if(!$user->state->remaining_friends_request_accept)
            throw new GQLException(('errors.unauthenticated'), 'you have received limit to accept friend requests for this month.');

        if($user->acceptFriendRequest($sender))
            return $request;

        return $this->responseWithError(__('errors.unauthenticated'), ['user' => 'Unauthenticated']);
    }


    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return string
     * @throws GQLException
     */
    public function reject_request($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $sender = User::find($args['sender_id']);

        if(!$sender)
            throw new GQLException(__('errors.unauthenticated'), 'Unauthenticated');

        /** @var User $user */
        $user = auth()->user();

        $friendship = \Hootlex\Friendships\Models\Friendship::whereRecipientId($user->id)
            ->whereSenderId($sender->id)->whereStatus(2)->first();

        if(!$friendship)
            throw new GQLException(__('errors.unauthenticated'), 'Not found');


        if($friendship->delete())
            return "Friend request rejected!";

        throw new GQLException(__('errors.unauthenticated'), 'Unauthenticated');
    }


    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return string
     * @throws GQLException
     */
    public function unfriend($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $friend = User::find($args['friend_id']);

        if(!$friend)
            throw new GQLException(__('errors.unauthenticated'), 'Unauthenticated');

        $friendship = auth()->user()->unfriend($friend);

        if(!$friendship)
            throw new GQLException(__('errors.unauthenticated'), 'Not found');
        else
            return "Success!";
    }


    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return string
     * @throws GQLException
     */
    public function friends_requests($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        return auth()->user()->getFriendRequests();
    }
}
