<?php

namespace App\GraphQL\Mutations;

use App\Core\Filters\ChallengesFilters;
use App\Core\Status;
use App\Events\ChallengeLeveraged;
use App\GraphQL\Exceptions\GQLException;
use App\Http\Resources\Invitation;
use App\Models\Challenge;
use App\Models\ChallengeRequest;
use App\Pipelines\Challenges\CanAcceptRequestAfterSeventyTwoHours;
use App\Pipelines\Challenges\NoLimitToAcceptRequest;
use App\Pipelines\Challenges\ShouldNotBeSender;
use App\User;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class FTFPChallenges
{
    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param mixed[] $args The arguments that were passed into the field.
     * @param \Nuwave\Lighthouse\Support\Contracts\GraphQLContext $context Arbitrary data that is shared between all fields of a single query.
     * @param \GraphQL\Type\Definition\ResolveInfo $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     * @throws GQLException
     */
    public function send_request($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        /** @var User $user */
        $user = auth()->user();

        if( $user->getFriendsCount() < setting('minimum_friends_for_challenge', 5) )
            throw new GQLException("Not Enough Friends to Send Request", 'You must have at least ' . setting('minimum_friends_for_challenge', 5) . ' friends to send invitations');

        if($user->id == $args['recipientId'])
            throw new GQLException(__('errors.unauthorized'), __('errors.recipient_id_should_not_be_same'));


        if(!$user->canSendChallengeRequest())
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.not_enough_initial_request_to_send_challenge_request')]]);

        $recipient = User::find($args['recipientId']);

        $challenge = Challenge::find($args['challengeId']);

        $challengeRequest = $user->sendChallengeRequest($recipient, $challenge);

        return (new Invitation($challengeRequest))->resource;
    }

    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param mixed[] $args The arguments that were passed into the field.
     * @param \Nuwave\Lighthouse\Support\Contracts\GraphQLContext $context Arbitrary data that is shared between all fields of a single query.
     * @param \GraphQL\Type\Definition\ResolveInfo $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     * @throws GQLException
     */
    public function accept_request($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        /** @var User $user */
        $user = auth()->user();

        /** @var ChallengeRequest $challengeRequest */
        $challengeRequest = ChallengeRequest::find($args['challengeRequestId']);

        if( $user->getFriendsCount() < setting('minimum_friends_for_challenge', 5) )
            throw new GQLException(__('Not Enough Friends to Send Request'), 'You must have at least ' . setting('minimum_friends_for_challenge', 5) . ' friends to send invitations');

        if($user->id == $challengeRequest->sender_id)
            throw new GQLException(__('errors.unauthorized'), __('errors.sender_id_should_not_be_same'));

        if($user->id != $challengeRequest->recipient_id)
            throw new GQLException(__('errors.unauthorized'), __('errors.unauthorized_to_accept_challenge_request'));

        if(!$user->canAcceptChallengeRequest())
            throw new GQLException(__('errors.unauthorized'), __('errors.not_enough_accept_request_to_accept_challenge_request'));

        /** check if he is reached to their limit to accept challenge in 72 hours */
        $totalChallengeAccepted = ChallengeRequest::where('recipient_id', auth()->id())
            ->where('status', Status::ACCEPTED)
            ->where("created_at", ">", now()->modify('-72 hours')->toDateTimeString())->count();

         if($totalChallengeAccepted >= setting('max_no_challenge_accepted_in_72_hr'))
             throw new GQLException(__('errors.unauthorized'), __('errors.reached_to_accept_challenge_request_limit'));

        /** check if he is passes the time limit to accept next challenge request */
        $timeLimit = ChallengeRequest::where('recipient_id', $user->id)
            ->whereNotNull('challenge_accepted_at')
            ->latest()->first();

         if($timeLimit && now() < $timeLimit->challenge_accepted_at->addHours(setting('time_to_accept_a_challenge')))
             throw new GQLException(__('errors.unauthorized'), __('errors.reached_to_accept_challenge_request_limit'));

        $challengeRequest->mark_as_accepted();

        return (new Invitation($challengeRequest))->resource;
    }

    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param mixed[] $args The arguments that were passed into the field.
     * @param \Nuwave\Lighthouse\Support\Contracts\GraphQLContext $context Arbitrary data that is shared between all fields of a single query.
     * @param \GraphQL\Type\Definition\ResolveInfo $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     * @throws GQLException
     */
    public function leverage_challenge($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        /** @var User $user */
        $user = auth()->user();

        $challenge = ChallengeRequest::findOrFail($args['challengeRequestId']);

        if($user->id == $challenge->sender_id)
            $challenge->leverage_user_id = $challenge->recipient_id;

        if($user->id == $challenge->recipient_id)
            $challenge->leverage_user_id = $challenge->sender_id;

        $challenge->earned_leverage_points = $challenge->leverage_points;

        $challenge->status = Status::COMPLETED;

        $challenge->save();

        event(new ChallengeLeveraged($challenge));

        return $challenge;
    }

    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param mixed[] $args The arguments that were passed into the field.
     * @param \Nuwave\Lighthouse\Support\Contracts\GraphQLContext $context Arbitrary data that is shared between all fields of a single query.
     * @param \GraphQL\Type\Definition\ResolveInfo $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     * @throws GQLException
     */
    public function reject_challenge($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        /** @var User $user */
        $user = auth()->user();

        $challengeRequest = ChallengeRequest::find($args['challengeRequestId']);

        if($challengeRequest->status != Status::PENDING)
            throw new GQLException(__('errors.unauthorized'), __('errors.request_is_already_accepted'));

        if($user->id == $challengeRequest->sender_id)
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.sender_id_should_not_be_same')]]);

        if($user->id != $challengeRequest->recipient_id)
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.unauthorized_to_accept_challenge_request')]]);

        $challengeRequest->mark_as_rejected();

        return $challengeRequest;
    }

    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param mixed[] $args The arguments that were passed into the field.
     * @param \Nuwave\Lighthouse\Support\Contracts\GraphQLContext $context Arbitrary data that is shared between all fields of a single query.
     * @param \GraphQL\Type\Definition\ResolveInfo $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     * @throws GQLException
     */
    public function submit_video($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        /** @var User $user */
        $user = auth()->user();

        $challenge = ChallengeRequest::findOrFail($args["challengeRequestId"]);

        return $challenge;

//        $path = $request->video->store('public/videos/exercises');
//
//        $challenge_data = $challenge->challenge_data;
//
//        foreach ($challenge_data['exercises'] as $index => $exercise)
//        {
//            if($exercise['id'] == $request->exercise_id){
//                $challenge_data['exercises'][$index]['uploadedVideos'][auth()->id()] = asset('storage/' . str_replace('public/', '', $path));
//            }
//        }
//
//        $challenge->challenge_data = $challenge_data;
//
//        $challenge->save();
//
//        return $challenge;
    }

}
