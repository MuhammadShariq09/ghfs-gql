<?php

namespace App\Providers;

use App\Core\NotificationType;
use App\Core\Notifications\PushNotifications;
use App\Events\ChallengeDeclaredResult;
use App\Events\ChallengeRequestAccepted;
use App\Events\ChallengeRequestRejected;
use App\Events\ChallengeRequestSent;
use App\Events\FeedbackSubmitted;
use App\Events\UserRegistered;
use App\Listeners\ChallengeDeclaredResultListener;
use App\Listeners\ChallengeRequestAcceptedListener;
use App\Listeners\ChallengeRequestRejectedListener;
use App\Listeners\ChallengeRequestSentListener;
use App\Listeners\FeedbackSubmittedListener;
use App\Listeners\UserRegisteredListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ChallengeRequestSent::class => [
            ChallengeRequestSentListener::class
        ],
        ChallengeRequestAccepted::class => [
            ChallengeRequestAcceptedListener::class
        ],
        ChallengeRequestRejected::class => [
            ChallengeRequestRejectedListener::class
        ],
        ChallengeDeclaredResult::class => [
            ChallengeDeclaredResultListener::class
        ],
        'App\Events\ChallengeVoted' => [
            'App\Listeners\ChallengeVotedListener',
        ],
        'App\Events\ChallengeRequestDelete' => [
            'App\Listeners\ChallengeRequestDeleteListener',
        ],
        'App\Events\ChallengeSubmitted' => [
            'App\Listeners\ChallengeSubmittedListener',
        ],
        'App\Events\ChallengeLeveraged' => [
            'App\Listeners\ChallengeLeveragedListener',
        ],
        UserRegistered::class => [
            UserRegisteredListener::class
        ],
        FeedbackSubmitted::class => [
            FeedbackSubmittedListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('friendships.sent', function ($sender, $recipient) {
            $pushNotifications = new PushNotifications();
            $pushNotifications->addDevice($recipient->device_id, $recipient->device_type);
            $pushNotifications->send(
                __('notifications.titles.sent_friend_request'),
                __('notifications.body.sent_friend_request', ['name' => $sender->name]),
                NotificationType::FRIENDSHIP,
                ['recipient_id' => $recipient->id]
            );
        });

//        Event::listen('friendships.sent', function ($sender, $recipient) {
//            $pushNotifications = new PushNotifications();
//            $pushNotifications->addDevice($recipient->device_id, $recipient->device_type);
//            $pushNotifications->send(
//                __('notifications.titles.sent_friend_request'),
//                __('notifications.body.sent_friend_request', ['name' => $sender->name]),
//                NotificationType::FRIENDSHIP,
//                ['recipient_id' => $recipient->id]
//            );
//        });
    }
}
