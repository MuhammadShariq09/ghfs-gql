<?php

namespace App\Events;

use App\Models\ChallengeRequest;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChallengeSubmitted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var ChallengeRequest
     */
    public $challengeRequest;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ChallengeRequest $challengeRequest)
    {

        $this->challengeRequest = $challengeRequest;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
