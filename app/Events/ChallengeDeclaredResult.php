<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChallengeDeclaredResult
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $challengeRequest;
    public $sender;
    public $recipient;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($challengeRequest, $sender = null, $recipient = null)
    {
        $this->challengeRequest = $challengeRequest;
        $this->sender = $sender;
        $this->recipient = $recipient;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
