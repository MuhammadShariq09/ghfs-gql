<?php

namespace App\Pipelines\Challenges;

use App\Core\Status;
use App\Models\ChallengeRequest;
use App\Pipelines\Pipe;
use Closure;

class ShouldNotBeSender implements Pipe {

    /**
     * @param $content
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($content, Closure $next)
    {
        $challenge = $content['challengeRequest'];

        if($challenge->sender_id == auth()->id())
            throw new \Exception(__("errors.sender_id_should_not_be_same"), 422);

        return  $next($content);
    }
}