<?php

namespace App\Pipelines\Challenges;

use App\Core\Status;
use App\Models\ChallengeRequest;
use App\Pipelines\Pipe;
use Closure;

class NoLimitToAcceptRequest implements Pipe {

    /**
     * @param $content
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($content, Closure $next)
    {
        if(auth()->user()->state->accept_requests < 1)
            throw new \Exception(__("errors.reached_to_accept_challenge_request_limit"), 422);

        return  $next($content);
    }
}