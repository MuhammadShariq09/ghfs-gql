<?php

namespace App\Models;

use App\Core\Traits\ImageableFill;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Exercise extends Model
{

    use ImageableFill;

    protected $fillable = [ 'title', 'sets', 'repeat', 'description', 'image', 'challenge_id', 'category_id'];

    // public function myfill($data)
    // {
    //     try {
    //         $data['image'] = upload_base_64_image($data['image'], 'exercises');
    //     }catch (\Exception $e) {
    //         unset($data['image']);
    //     }

    //     $this->fill($data);
    // }

    public function challenge(): BelongsTo {
        return $this->belongsTo(Challenge::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
