<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voting extends Model
{
    protected $table = 'votings';

    protected $fillable = [ 'challenge_id', 'voter_id', 'vote_for', 'voting_points' ];
}
