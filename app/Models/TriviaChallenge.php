<?php

namespace App\Models;

use App\Core\Traits\ImageableFill;
use App\Core\Traits\Payloggable;
use App\TriviaQuestion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TriviaChallenge extends Model
{
    use ImageableFill, SoftDeletes, Payloggable;

    protected $fillable = ["title", "description", "time", "type", "points", "prize", "prize_value", "image", "amount"];

//    protected $appends = ['image'];

    public $imageFolder = 'trivia/challenges';

    public function questions()
    {
        return $this->hasMany(TriviaQuestion::class, 'challenge_id');
    }

    public function saveQuestions($questions)
    {
        $this->questions()->whereNotIn('id', collect($questions)->pluck('id'))->delete();

        foreach($questions as $question)
        {
            if(array_key_exists('id', $question))
            {
                $questionModel = TriviaQuestion::find($question['id']);
                $questionModel->fill($question);
                $questionModel->save();
                continue;
            }

            $questionModel = new TriviaQuestion();
            $questionModel->fill($question);
            $this->questions()->save($questionModel);
            $questionModel->saveAnswers($question['answers']);
        }
    }
}
