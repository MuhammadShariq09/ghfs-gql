<?php

namespace App\Models;

use App\TriviaQuestion;
use Illuminate\Database\Eloquent\Model;

class ChallengeAnswer extends Model
{
    protected $guarded = [];

    public function question()
    {
        return $this->belongsTo(TriviaQuestion::class);
    }
}
