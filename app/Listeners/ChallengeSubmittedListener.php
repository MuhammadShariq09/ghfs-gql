<?php

namespace App\Listeners;

use App\Core\Notifications\PushNotifications;
use App\Core\NotificationType;
use App\Core\Status;
use App\Events\ChallengeSubmitted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChallengeSubmittedListener
{
    /**
     * @var PushNotifications
     */
    private $pushNotifications;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PushNotifications $pushNotifications)
    {
        $this->pushNotifications = $pushNotifications;
    }

    /**
     * Handle the event.
     *
     * @param  ChallengeSubmitted  $event
     * @return void
     */
    public function handle(ChallengeSubmitted $event)
    {
        $challenge = $event->challengeRequest;
        $sender = $challenge->sender;
        $recipient = $challenge->recipient;

        $points = (int)setting('video_posted_points',0) * count($challenge->challenge_data['exercises']);

        if(auth()->id() == $challenge->sender_id)
        {
            $senderPoints = $challenge->sender_points ?? [];
            $senderPoints['video_submission'] = $points;
            $challenge->sender_points = $senderPoints;
        }

        if(auth()->id() == $challenge->recipient_id)
        {
            $recipientPoints = $challenge->sender_points ?? [];
            $recipientPoints['video_submission'] = $points;
            $challenge->recipient_points = $recipientPoints;
        }

        $challenge->save();

        /** @var \App\User $currentUser */
        $currentUser = auth()->user();
        $currentUser->state->increment('ftfp_points', $points);
        $currentUser->incrementBonusPoints();

        if($currentUser->id == $sender->id) { $device_id = $recipient->device_id; $device_type = $recipient->device_type; }

        if($currentUser->id == $recipient->id) { $device_id = $sender->device_id; $device_type = $sender->device_type; }

        $this->pushNotifications->addDevice($device_id, $device_type);

        $this->pushNotifications->send(
            __('notifications.titles.challenge_submitted'),
            __('notifications.body.challenge_submitted', ['name' => $currentUser->name]),
            NotificationType::CHALLENGE,
            ['challenge_id' => $challenge->id, 'view' => 3, 'type' => 'challenge']
        );
    }
}
