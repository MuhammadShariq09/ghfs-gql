<?php

namespace App\Listeners;

use App\Core\Notifications\PushNotifications;
use App\Core\NotificationType;
use App\Core\Status;
use App\Models\Administrator\Admin;
use App\Models\ChallengeRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChallengeDeclaredResultListener
{
    /**
     * @var PushNotifications
     */
    private $pushNotifications;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PushNotifications $pushNotifications)
    {
        $this->pushNotifications = $pushNotifications;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $challenge = $event->challengeRequest;
        $sender = $event->sender ?? $challenge->sender;
        $recipient = $event->recipient ?? $challenge->recipient;

        $winner = null;
        $losser = null;

        if( $challenge->recipient_result == Status::WINNER )
        {
            $winner = $recipient;
            $losser = $sender;
        }

        if( $challenge->sender_result == Status::WINNER )
        {
            $losser = $recipient;
            $winner = $sender;
        }

        // Increment Points.
        if($winner && $sender && $challenge->sender_submitted && $challenge->recipient_submitted)
        {
            $points = (int)$challenge['challenge_data']['reward_points'];
            $sender->state
                ->increment('ftfp_points', $points);
        }

        $senderCount = ChallengeRequest::where(function($q) use($sender){
            $q->orWhere('sender_id', auth()->id())
                ->orWhere('recipient_id', auth()->id());
        })->where('status', Status::IN_VOTING)
            ->whereIn('sender_result', [Status::WINNER, Status::DEFEATED])
            ->whereIn('recipient_result', [Status::WINNER, Status::DEFEATED])
            ->count();

        $recipientCount = ChallengeRequest::where(function($q) use($recipient){
            $q->orWhere('sender_id', auth()->id())
                ->orWhere('recipient_id', auth()->id());
        })->where('status', Status::IN_VOTING)
            ->whereIn('sender_result', [Status::WINNER, Status::DEFEATED])
            ->whereIn('recipient_result', [Status::WINNER, Status::DEFEATED])
            ->count();

        $sender->state()->update(['count_unverified_challenges' => $senderCount]);
        $recipient->state()->update(['count_unverified_challenges' => $recipientCount]);

        if($challenge->status == Status::IN_VOTING)
            $challenge->update(['recipient_seen' => 0, 'sender_seen' => 0]);

        if(auth()->user() instanceof Admin){}
        else
            auth()->user()->saveLog(__('log_messages.you_declared_challenge', ['challenge_name' => $challenge->challenge_data['title']]));

        $this->sendPushNotification($event, $sender, $recipient);
    }

    /**
     * @param $event
     * @param $sender
     * @param $recipient
     */
    protected function sendPushNotification($event, $sender, $recipient): void
    {
        if (auth()->id() == $sender->id) {
            $device_id = $recipient->device_id;
            $device_type = $recipient->device_type;
        }

        if (auth()->id() == $recipient->id) {
            $device_id = $sender->device_id;
            $device_type = $sender->device_type;
        }

        if(auth()->user() instanceof Admin)
        {
            $this->pushNotifications->addDevice($recipient->device_id, $recipient->device_type);
            $this->pushNotifications->addDevice($sender->device_id, $sender->device_type);

            $this->pushNotifications->send(
                __('notifications.titles.challenge_declared_result'),
                __('notifications.body.challenge_declared_result'),
                NotificationType::CHALLENGE,
                ['challenge_id' => $event->challengeRequest->id]
            );

        }else{
            $this->pushNotifications->addDevice($device_id, $device_type);


            $viewCode = 5;

            if($event->challengeRequest->status == Status::SUBMITTED)
                $viewCode = 3;
            if($event->challengeRequest->status == Status::IN_VOTING)
                $viewCode = 4;


            $this->pushNotifications->send(
                __('notifications.titles.challenge_declared_result'),
                __('notifications.body.challenge_declared_result'),
                NotificationType::CHALLENGE,
                ['challenge_id' => $event->challengeRequest->id, 'type' => 'challenge', 'view' => $viewCode]
            );
        }

    }
}
