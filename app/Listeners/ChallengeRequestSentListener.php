<?php

namespace App\Listeners;

use App\Core\Notifications\PushNotifications;
use App\Core\NotificationType;
use App\Events\ChallengeRequestSent;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChallengeRequestSentListener
{
    /**
     * @var PushNotifications
     */
    private $pushNotifications;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PushNotifications $pushNotifications)
    {
        //
        $this->pushNotifications = $pushNotifications;
    }

    /**
     * Handle the event.
     *
     * @param  ChallengeRequestSent  $event
     * @return void
     */
    public function handle(ChallengeRequestSent $event)
    {
        // Generate Log to user
        auth()->user()->saveLog(__('log_messages.send_challenge_request', ['recipient_name' => $event->challengeRequest->recipient->name]));

        $recipient = $event->challengeRequest->recipient;

        $sender = $event->challengeRequest->sender;


        $recipient->notify(new \App\Notifications\ChallengeRequestSent($event->challengeRequest, $sender));

        $this->pushNotifications->addDevice($recipient->device_id, $recipient->device_type);
        $this->pushNotifications->send(
            __('notifications.titles.challenge_sent'),
            __('notifications.body.challenge_sent', ['name' => $sender->name]),
            NotificationType::CHALLENGE,
            ['challenge_id' => $event->challengeRequest->id, 'view' => 2]
        );

        // dd($this->pushNotifications);

    }
}
