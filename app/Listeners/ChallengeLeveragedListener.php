<?php

namespace App\Listeners;

use App\Core\Notifications\PushNotifications;
use App\Events\ChallengeLeveraged;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChallengeLeveragedListener
{
    /**
     * @var PushNotifications
     */
    private $pushNotifications;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PushNotifications $pushNotifications)
    {
        $this->pushNotifications = $pushNotifications;
    }

    /**
     * Handle the event.
     *
     * @param  ChallengeLeveraged  $event
     * @return void
     */
    public function handle(ChallengeLeveraged $event)
    {
        $challenge = $event->challengeRequest;
        /** @var User $sender*/
        $sender = $challenge->sender;
        /** @var User $recipient*/
        $recipient = $challenge->recipient;

        if($sender->id == auth()->id()){
            $recipient->state->increment('ftfp_points', $challenge->leverage_points);
            $recipient->incrementBonusPoints();
        }

        if($recipient->id == auth()->id()){
            $sender->state->increment('ftfp_points', $challenge->leverage_points);
            $sender->incrementBonusPoints();
        }
    }
}
