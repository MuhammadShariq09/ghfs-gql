<?php

namespace App\Console\Commands;

use App\Core\Status;
use App\Events\ChallengeRequestDelete;
use App\Models\ChallengeRequest;
use Illuminate\Console\Command;

class RemoveUnAcceptedChallengeRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'challenge:remove_unaccepted_request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $acceptableHours = now()->subHours(setting('time_to_accept_a_challenge'))->toDateTimeString();

        ChallengeRequest::whereStatus(Status::PENDING)
            ->where('created_at', '<', $acceptableHours)->delete();
    }
}
