<?php

namespace App\Console\Commands;

use App\Models\State;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ResetUserStatsEveryMonth extends Command
{
    protected $signature = 'userstats:reset';

    protected $description = 'This Command will reset user\'s allowed request and as per admin settings';

    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $this->resetUsersStats();
        $this->info('User\'s States are reset successfully');
    }

    private function resetUsersStats()
    {
        State::query()->decrement('remaining_days');

        $data = [
            'initial_requests'=> setting('allowed_requests_monthly'),
            'accept_requests'=> setting('allowed_requests_monthly'),
            'remaining_days'=> setting('reset_days'),
            'remaining_friends_request_accept'=> setting('maximum_friends_request_accept_in_a_month', 20),
            'remaining_friends_request_accept'=> setting('maximum_friends_request_accept_in_a_month', 20),
        ];

        State::query()->where('remaining_days', 0)->update($data);
    }
}
