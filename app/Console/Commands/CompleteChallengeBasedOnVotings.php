<?php

namespace App\Console\Commands;

use App\Core\Status;
use App\Events\ChallengeRequestDelete;
use App\Models\ChallengeRequest;
use App\Notifications\ChallengeLost;
use App\Notifications\ChallengeWon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CompleteChallengeBasedOnVotings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'challenge:mark_completed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->changeChallenges();
    }

    private function changeChallenges()
    {
        $challenges = ChallengeRequest::with('votes')->whereStatus(Status::IN_VOTING)
            ->where('voting_expired_at', '<', now()->toDateTimeString())->get();

        foreach ($challenges as $challenge)
        {
            $senderVotes = $challenge->votes->where('vote_for', $challenge->sender_id)->count();

            $recipientVotes = $challenge->votes->where('vote_for', $challenge->recipient_id)->count();
//            dd($recipientVotes, $senderVotes);
            switch (true)
            {
                case $recipientVotes > $senderVotes:
                        $challenge->status = Status::COMPLETED;
                        $challenge->recipient_result = Status::WINNER;
                        $challenge->sender_result = Status::DEFEATED;

                        try{
                            $challenge->recipient->notify(new ChallengeWon($challenge->challenge->title));
                            $challenge->sender->notify(new ChallengeLost($challenge->challenge->title));
                        }catch(\Exception $exception){
                            Log::critical("Email is not send from voting command " . $exception->getMessage());
                        }
                    break;
                case $senderVotes > $recipientVotes:
                        $challenge->status = Status::COMPLETED;
                        $challenge->sender_result = Status::WINNER;
                        $challenge->recipient_result = Status::DEFEATED;

                        try{
                            $challenge->sender->notify(new ChallengeWon($challenge->challenge->title));
                            $challenge->recipient->notify(new ChallengeLost($challenge->challenge->title));
                        }catch(\Exception $exception){
                            Log::critical("Email is not send from voting command " . $exception->getMessage());
                        }
                    break;
                default:
                    $challenge->status = Status::DISPUTE;
            }

//            $challenge->save();
        }
    }


}
