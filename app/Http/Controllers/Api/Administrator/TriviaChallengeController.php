<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Http\Controllers\Controller;
use App\Models\TriviaChallenge;
use Illuminate\Http\Request;

class TriviaChallengeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TriviaChallenge::paginate(setting('default_pagination_length', 25));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->roles());

        $challenge = new TriviaChallenge();

        $challenge->fill($request->only($challenge->getFillable()));

        auth()->user()->trivia_challenges()->save($challenge);

        $challenge->saveQuestions($request->questions);

        $challenge->load('questions');

        return response()->json($challenge);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($challengeId)
    {
        $challenge = TriviaChallenge::find($challengeId);

        $challenge->load('answers');

        return response()->json($challenge);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  TriviaChallenge  $challenge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $challengeId)
    {
        $request->validate($this->roles());

        $challenge = TriviaChallenge::find($challengeId);

        $challenge->fill($request->only($challenge->getFillable()));

        $challenge->save();

        $challenge->saveAnswers($request->answers);

        $challenge->load('answers');

        return response()->json($challenge);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($challengeId)
    {
        $challenge = TriviaChallenge::find($challengeId);
        $challenge->delete();
        return response()->json(['deleted' => true]);
    }

    private function roles()
    {
        return [
            "title" => "required",
            "description" => "string",
            "time" => "required|numeric",
            "points" => "required|numeric|min:1",
            "image" => "required|string",
            "prize" => "required",
            "prize_value" => ["required", function ($attribute, $value, $fail) {
                if (strtolower(request('prize')) === 'cash') {
                    if(!is_numeric($value))
                        $fail(str_replace('_', ' ', ucfirst($attribute)).' only numeric value is allowed.');
                }
            }],
            "questions" => "required|array|max:" . setting('maximum_questions_per_trivia_challenge', 25),
            "questions.*.title" => "required|string",
            "questions.*.answers" => "required|array",
            "questions.*.answers.*.title" => "required",
            "questions.*.answers.*.correct" => "required",
        ];
    }
}
