<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Http\Controllers\Api\ApiBaseController;
use App\Models\PasswordReset;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class PasswordResetController extends ApiBaseController
{
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|exists:users',
        ]);

        $user = User::where('email', $request->email)->first();

        $passwordReset = PasswordReset::updateOrCreate(['email' => $user->email], [ 'email' => $user->email, 'token' => Str::random(8)]);


        if ($user && $passwordReset)
            $user->notify(new PasswordResetRequest($passwordReset));

        return response()->json(['message' => 'We have e-mailed your password reset link!']);
    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function validateToken($token)
    {
        $passwordReset = PasswordReset::where('token', $token)->first();

        if (!$passwordReset)
            return $this->responseWithError('Unprocessable entities.', ['token' => ['Invalid Token']]);

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();

            return $this->responseWithError('Unprocessable entities.', ['token' => ['This password reset token is invalid.']]);
        }

        return $passwordReset;
    }
     /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|exists:users',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);

        $passwordReset = PasswordReset::where(['token' => $request->token, 'email' => $request->email])->first();

        if (!$passwordReset)
            return $this->responseWithError("Unprocessable Entities", ['token' => ["This password reset token is invalid."]]);

        $user = User::where('email', $passwordReset->email)->first();

        if (!$user)
            return $this->responseWithError("Unprocessable Entities", ['token' => ["We can't find a user with that e-mail address"]]);

        $user->password = bcrypt($request->password);

        $user->save();

        $passwordReset->delete();

        $user->notify(new PasswordResetSuccess($passwordReset));

        return response()->json($user);
    }
}
