<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Http\Controllers\Api\ApiBaseController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;

class NotificationsController extends ApiBaseController
{
    public function index()
    {
        /** @var User $user */
        $user = request()->user();

        return $this->response([
            'notifications' => $user->notifications()->latest()->paginate(3),
            'unread_notifications_count' => $user->unreadNotifications()->count(),
        ]);
    }

    public function readNotification($notificationId)
    {
        $notification = DatabaseNotification::find($notificationId);

//        $notification->markAsRead();

        return $this->response(['message' => "Notification read"]);
    }
}
