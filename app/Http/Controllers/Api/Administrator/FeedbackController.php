<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Core\Filters\FeedbackFilters;
use App\Http\Controllers\Api\ApiBaseController;
use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends ApiBaseController
{
    public function index(Request $request, FeedbackFilters $filters)
    {
        return Feedback::filter($filters)->latest()->with('owner')->paginate();
    }
}
