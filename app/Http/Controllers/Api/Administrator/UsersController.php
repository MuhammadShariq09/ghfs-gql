<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Core\Filters\UserFilters;
use App\Http\Resources\User as UsersCollection;
use App\Notifications\UserStatusChanged;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    function __construct()
    {
        return $this->middleware('bindings');
    }

    function index(Request $request, UserFilters $filters)
    {
        return UsersCollection::collection(User::filter($filters)->with('state')->latest()->paginate($request->per_page));
    }

    function store(Request $request)
    {
        $user = new User();

        $data = $request->only($user->getFillable());
        $data['password'] = bcrypt($data['password']);

        $user->fill($data);
        $user->save();
        $user->addState();
        return response()->json($user);
    }

    function show($user)
    {
        /** @var User $user */
        $user = User::findOrFail($user);
        $user->load('state');

        $user->load(['challenges' => function($q){ $q->with('sender', 'recipient'); }]);

        $user->showWithChallenge = true;

        return response()->json(new \App\Http\Resources\User($user));
    }

    function update(Request $request, $user)
    {
        /** @var User $user */
        $user = User::findOrFail($user);

        $this->bcryptPassword($request);

        $user->fill($request->only($user->getFillable()));

        $user->save();

        return response()->json(new \App\Http\Resources\User($user));
    }

    private function bcryptPassword(Request $request)
    {
        if ($request->filled('password')) {
            return $request->merge(['password' => bcrypt($request->password)]);
        }

        unset($request['password']);
    }

    function profile(Request $request)
    {
        $user = $request->user();

        $user->load(['state', 'logs' => function($q) { $q->latest()->limit(25); }]);

        return response()->json(($user));
    }

    function friends($user)
    {
        $user = User::findOrFail($user);

        return response()->json($user->getFriends()->load('state'));
    }

    function statusPut(User $user)
    {
        $user->update(['status' => ! $user->status]);
        $user->notify(new UserStatusChanged());
        return response()->json(new \App\Http\Resources\User($user));
    }

    function destroy(Request $request)
    {
        return dd(Storage::disk('public')->delete((file_get_contents('php://input'))));
    }
}
