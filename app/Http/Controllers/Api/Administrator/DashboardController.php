<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Core\Status;
use App\Models\ChallengeRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    function index()
    {
        $years = \DB::select("select distinct year(created_at) as year from " . (new User)->getTable());

        $users = User::query()->select('created_at')->where('created_at','>', now()->subYear()->endOfMonth()->toDateTimeString())->get()
            ->groupBy(function($item) {
                return $item->created_at->month;
            });

        $userChart = collect([]);

        for ($month = 1; $month <= 12; $month++)
        {
            if($users->has($month))
                $userChart->push($users->get($month)->count());
            else
                $userChart->push(0);
        }

        return [
            'total_users' => User::count(),
            'recent_users' => \App\Http\Resources\User::collection(User::with('state')->latest()->limit(5)->get()),
            'active_challenges' => ChallengeRequest::where('status', Status::ACCEPTED)->count(),
            'income' => rand(500, 1000),
            'lead_boards' => \App\Http\Resources\User::collection(User::inRandomOrder()->limit(3)->get()),
            'boards' => \App\Http\Resources\User::collection(User::inRandomOrder()->limit(25)->paginate()),
            'users_chart_data' => $userChart,
            'available_years' => collect($years)->map(function($q){ return $q -> year; })->toArray(),
        ];
    }
}
