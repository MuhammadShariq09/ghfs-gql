<?php

namespace App\Http\Controllers\Api;

use App\Core\Filters\UserFilters;
use App\Core\Status;
use App\Http\Resources\UsersCollection;
use App\Models\ChallengeRequest;
use App\User;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UsersController extends ApiBaseController
{

    public function __construct()
    {
        $this->middleware('bindings');
    }

    function index(Request $request, UserFilters $filters)
    {
        /** @var User $user */
        $user = auth()->user();

        // Filter User by name or return all but paginated data
        $users = User::with(['state'])->filter($filters)->paginate(setting('pagination_length', 25));

        // get all user's IDs so that
        $userIds = $users->pluck('id');

        $friendsStatus = Friendship::query()->where(function($q) use($userIds){
            $q->whereIn('sender_id', $userIds)
                ->where('recipient_id', auth()->id());
        })->orWhere(function($q) use($userIds){
            $q->whereIn('recipient_id', $userIds)
                ->where('sender_id', auth()->id());
        })->get();

        $users->each(function(&$user, $index) use($friendsStatus){
            $f = $friendsStatus->filter(function($friendShip) use($user){
                return ($friendShip->sender_id == $user->id && $friendShip->recipient_id == auth()->id())
                    ||
                    $friendShip->sender_id == auth()->id() && $friendShip->recipient_id == $user->id;
            })->first();

            $user->friend_status = $f;
        });

        return \App\Http\Resources\User::collection($users);
    }

    function show($user)
    {
        $user = User::findOrFail($user);
        $user->load('state');
        return response()->json(($user));
    }

    function logs()
    {
        return response()->json(auth()->user()->logs()->paginate(setting('pagination_length', 25)));
    }

    function payLogs()
    {
        return response()->json(auth()->user()->transactions()->paginate(setting('pagination_length', 25)));
    }

    function payLogsShow($transaction_id)
    {
        return response()->json(auth()->user()->transactions()->where('id', $transaction_id)->first());
    }

    function profile(Request $request)
    {
        /** @var User $user*/
        $user = null;

        if($userId = request('id'))
            $user = User::find($userId);
        else
            $user = $request->user();

        // \DB::listen(function($d){ dd($d); });
        $user->setBadgeCount();

        $user->getChallengeVotingRequestCount();

        $user->getChallengesCount();

        $user->load(['state', 'logs' => function($q) { $q->paginate(setting('pagination_length', 25)); }]);

        return response()->json(($user));
    }

    function profilePut(Request $request)
    {
        $user = $request->user();

        try {
            $this->bcryptPassword($request);
        } catch (\Exception $e) {
            return $this->responseWithError('The given data is invalid,', ['password' => [$e->getMessage()]]);
        }

        $user->fill($request->only(["name","address","password","country","city","address_state","postal_code","contact","image"]));
        $user->save();
        $user->saveLog(__('user.logs.profile_updated'));
        return response()->json(($user));
    }

    private function bcryptPassword(Request $request)
    {
        if ($request->filled('password')) {

            if(! Hash::check($request->old_password, auth()->user()->password))
            {
                throw new \Exception('Password doesn\'t match');
            }

            return $request->merge(['password' => bcrypt($request->password)]);
        }

        unset($request['password']);
    }

    function friends(User $user, UserFilters $filters)
    {
        $friends = $user->getFriendsWithFilters($filters, setting('pagination_length', 25));

        $userIds = $friends->pluck('id');

        $friendsStatus = Friendship::query()->where(function($q) use($userIds){
            $q->whereIn('sender_id', $userIds)
                ->where('recipient_id', auth()->id());
        })->orWhere(function($q) use($userIds){
            $q->whereIn('recipient_id', $userIds)
                ->where('sender_id', auth()->id());
        })->get();


        $friends->each(function(&$user, $index) use($friendsStatus){
            $f = $friendsStatus->filter(function($friendShip) use($user){
                return ($friendShip->sender_id == $user->id && $friendShip->recipient_id == auth()->id())
                    ||
                    $friendShip->sender_id == auth()->id() && $friendShip->recipient_id == $user->id;
            })->first();

            $user->friend_status = $f;
        });

        return response()->json(\App\Http\Resources\User::collection($friends));
    }

    function statusPut(User $user)
    {
        $user->save(['status' => ! $user->status]);
        return response()->json(new \App\Http\Resources\User($user), 402);
    }
}
