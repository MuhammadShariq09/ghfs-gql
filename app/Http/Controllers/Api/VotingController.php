<?php

namespace App\Http\Controllers\Api;

use App\Core\Status;
use App\Http\Controllers\Controller;
use App\Http\Resources\Invitation;
use App\Models\ChallengeRequest;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Http\Request;

class VotingController extends Controller
{
    function index(Request $request)
    {
        $ids = auth()->user()->getFriendsIds();

        $challenge = ChallengeRequest::whereStatus(Status::IN_VOTING)
            ->where(function($q) use($ids){
                $q->orWhereIn('sender_id', $ids);
                $q->orWhereIn('recipient_id', $ids);
            })
            ->where('sender_id', '!=', auth()->id())
            ->where('recipient_id', '!=', auth()->id())
            ->with('sender.state', 'recipient.state')
            ->paginate(setting('pagination_length', 25));

        $challenge->each(function($c){ return $c->prepare(); });

        return $challenge;
    }
    function store(Request $request)
    {
        $request->validate([
            'challenge_id' => 'required|exists:challenge_requests,id',
            'user_id' => 'required|exists:users,id'
        ]);

        $challenge = ChallengeRequest::findOrFail($request->challenge_id);

        return auth()->user()->addVote($challenge, $request->user_id);
    }
}
