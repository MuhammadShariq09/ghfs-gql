<?php

namespace App\Http\Controllers\Api;

use App\Events\FeedbackSubmitted;
use App\Models\Feedback;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbackController extends ApiBaseController
{
    public function store(Request $request)
    {
        $request->validate([
            'email' => "required|email",
            "subject" => "required",
            "message" => "required"
        ]);

        $feedback = new Feedback();
        $feedback->fill($request->only($feedback->getFillable()));

        /** @var User $user */
        $user = auth()->user();
        $user->feedbacks()->save($feedback);

        event(new FeedbackSubmitted($feedback));

        return $this->response(['message'=>'Your request has been sent successfully']);
    }
}
