<?php

namespace App\Http\Controllers\Api;

use App\Core\Support\Services\Payments\StripeChargeService;
use App\Models\Transaction;
use App\Models\TriviaChallenge;
use App\Models\UserTriviaChallenge;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class TriviaChallengeController extends ApiBaseController
{
    public function index()
    {
        $query = TriviaChallenge::query();

        if(strtolower(request('paid')) == 'paid')
            $query->where('type', ucwords(request('paid')));

        $query->with('questions.answers');

        return response()->json($query->paginate());
    }

    public function start(Request $request, StripeChargeService $paymentService)
    {
        $request->validate([
            'challenge_id' => 'required|exists:trivia_challenges,id',
            'type' => 'required|in:free,paid',
        ]);

        if(strtolower($request->type) == 'paid')
        {
            $request->validate([
                "number" => 'required|digits:16',
                "exp_month" => 'required|date_format:m',
                "exp_year" => 'required|date_format:Y',
                "cvc" => 'required'
            ]);
        }

        $challenge = TriviaChallenge::find($request->challenge_id);
        $challenge->load('questions.answers');

        /** @var User $user*/
        $user = auth()->user();

//        if($user->alreadyAttempted($challenge))
//            return $this->responseWithError('already taken the exam', ['challenge' => ['Already Taken this exam']]);

        $result = DB::transaction(function () use($user, $challenge, $request){

            $user->triviaChallenges()->attach($request->challenge_id, [
                'challenge_data' => $challenge,
                'total_questions' => $challenge->questions()->count(),
                'attempted_questions' => 0,
                'correct_questions' => 0,
                'wrong_questions' => 0,
                'earned_points' => 0,
                'completed' => 0,
                'duration' => $challenge->time,
                'paid' => strtolower($challenge->type) === "paid",
                'ended_at' => now()->addMinutes($challenge->time)->toDateTimeString(),
            ]);

            if($request->type == 'paid')
            {
                try{

                    $user->createOrGetStripeCustomer();

                    $user->addPaymentMethod($request->only('number', 'cvc', 'exp_month', 'exp_year'));

                    $charge = $user->charge($challenge->amount * 100, $user->cards()->where('default', true)->first()->stripe_pm_id);

                    $challenge->savePayLog($challenge->amount, "Cost Trivia Challenge", $charge->id);

                }catch (\Exception $exception){
                    DB::rollBack();
                    return $this->responseWithError(['message' => $exception->getMessage()]);
                }
            }

            return UserTriviaChallenge::whereUserId(auth()->id())->latest()->first();

        });

        return $result;
    }

    public function checkAnswer(Request $request)
    {
        $request->validate([
            'challenge_id' => 'required|exists:user_trivia_challenges,id',
            'question_id' => 'required',
            'correct' => 'required|min:0,max:1'
        ]);

        $challenge = UserTriviaChallenge::find($request->challenge_id);

        $challengeDetails = ($challenge->challenge_data);
        $questions = ($challengeDetails['questions']);

        foreach($questions as $index => $question)
        {
            if($question['id'] == $request->question_id)
            {
                $questions[$index]['attempted'] = true;
                $questions[$index]['correct'] = $request->correct;
            }
        }

        $questions = collect($questions);

        $challenge->attempted_questions = $questions->filter(function($q){
            return array_key_exists('attempted', $q) && $q['attempted'];
        })->count();
        $challenge->correct_questions = $questions->filter(function($q){
            return array_key_exists('correct', $q) && $q['correct'];
        })->count();
        $challenge->wrong_questions = $questions->filter(function($q){
            return array_key_exists('correct', $q) && !$q['correct'];
        })->count();

        $challengeDetails['questions'] = $questions->toArray();
        $challenge->challenge_data = $challengeDetails;

        $challenge->save();

        return $challenge;
    }

    public function finish(Request $request)
    {
        $request->validate([
            'challenge_id' => 'required|exists:user_trivia_challenges,id',
        ]);

        $challenge = UserTriviaChallenge::find($request->challenge_id);

        $challenge_data = $challenge->challenge_data;

        $questions = collect($challenge_data['questions']);

        $points = $questions->filter(function($q){
            return array_key_exists('correct', $q) && $q['correct'];
        })->count() * $challenge_data['points'];

        $challenge->earned_points = $points;

        $challenge->completed = true;

        auth()->user()->state()->increment('points', $points);

         $challenge->save();

        return $challenge;
    }

    public function running(Request $request)
    {
        $challenge = UserTriviaChallenge::whereUserId(auth()->id())->whereCompleted(0)->latest()->first();

        if($challenge)
        {
            $challenge->remaining_time = now()->diffInMinutes($challenge->ended_at);

            if(now() > $challenge->ended_at)
            {
                $challenge->remaining_time = 'expired';
            }
        }

        return $challenge ?? $this->response(['message' => 'No Challenge Found']);
    }
}
