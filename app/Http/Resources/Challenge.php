<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Challenge extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'path' => $this->path,
            'level' => $this->level,
            'title' => $this->title,
            'description' => $this->description,
            'reward_points' => $this->reward_points,
            'image' => $this->image,
            'exercises' => $this->exercises,
            'created_at' => $this->created_at->format('d/m/Y h:i:s'),
            'updated_at' => $this->updated_at,
        ];
    }
}
