<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChallengeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'path' => $this->path,
            'image' => $this->image,
            'title' => $this->title,
            'level' => $this->level,
            'status' => $this->status,
            'duration' => $this->duration,
            'description' => $this->description,
            'reward_points' => $this->reward_points,
            'exercises' => ExerciseResource::collection($this->exercises),
            'created_at' => $this->created_at->format('d/m/Y h:i:s'),
            'updated_at' => $this->updated_at->format('d/m/Y h:i:s')
        ];
    }
}
