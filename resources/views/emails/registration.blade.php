@extends('emails.master')

@section('title', "Password Reset Request " . env('app.name'))

@section('content')

    @if(isset($admin))
        <p>A new User has Signed up for {{ config('app.name') }} :)</p>

        To view the details of user <a href="{{ route('admin.login') }}">please log</a> in to your admin panel.
    @else
    <h1>Hi,  {{ $user->name }} <br></h1>

    <p>Your email ({{ $user->email }}) have been registered in {{ config('app.name') }}.</p>
    @endif

    <table align="center" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td align="center">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

@endsection
