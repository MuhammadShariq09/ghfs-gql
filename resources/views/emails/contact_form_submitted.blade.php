@extends('emails.master')

@section('title', "Password Reset Request " . env('app.name'))

@section('content')

    @if(isset($admin))

        <p>New Query form is submitted</p>

        To view the details of user <a href="{{ route('admin.login') }}">please log</a> in to your admin panel.

    @else

    <h1>Hi,  {{ $user->name }} <br></h1>

    <p>Thank you for submitting query to us, we will get back to you soon.</p>
    @endif

    <table align="center" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td align="center">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

@endsection
