import VueRouter from "vue-router";

import DashboardComponent from './compoments/DashboardComponent';
import EditProfileComponent from './compoments/users/EditProfileComponent';
import SettingsComponent from './compoments/SettingsComponent';
import UserIndex from './compoments/users/IndexComponent';
import UserCreate from './compoments/users/CreateComponent';
import UserShow from './compoments/users/ShowComponent';
import ChallengesIndex from './compoments/challenges/IndexComponent';
import ChallengesCreate from './compoments/challenges/CreateComponent';
import ChallengesShow from './compoments/challenges/ShowComponent';
import NotFound from './compoments/NotFoundComponent';
import FitToPathCreate from './compoments/fit-to-path/CreateComponent';
import FeedbackIndex from './compoments/feedback/IndexComponent';
import PoolingRequestsIndex from './compoments/pooling-requests/IndexComponent';
import PoolingRequestsShow from './compoments/pooling-requests/ShowComponent';
import TriviaChallengeIndex from './compoments/trivia-challenges/Index';


// import eChartsComponent from "../compoments/eChartsComponent";

const router = new VueRouter({
    mode: 'history',
    base: `${process.env.NODE_ENV === 'production' ? process.env.MIX_PRODUCTION_VUE_ROUTER_BASE_URL: process.env.MIX_VUE_ROUTER_BASE_URL}/admin`,
    routes: [
        {
            path: '*',
            name: 'notfound',
            component: NotFound,
            meta: {
                title: "Not found",
                description: ""
            }
        },
        {
            path: '/dashboard',
            name: 'home',
            component: DashboardComponent,
            meta: {
                title: "Dashboard",
                description: ""
            }
        },
        {
            path: '/profile/:id',
            name: 'profile.edit',
            component: UserCreate,
            meta: {
                title: "Profile",
                description: ""
            }
        },
        {
            path: '/settings',
            name: 'settings',
            component: SettingsComponent,
            meta: {
                title: "Settings",
                description: ""
            }
        },
        {
            path: '/users',
            name: 'users.index',
            component: UserIndex,
            meta: {
                title: "Users List",
                description: ""
            }
        },
        {
            path: '/users/create',
            name: 'users.create',
            component: UserCreate,
            meta: {
                title: "Add User",
                description: ""
            }
        },
        {
            path: '/users/:id/',
            name: 'users.edit',
            component: UserCreate,
            meta: {
                title: "Edit User",
                description: ""
            }
        },
        {
            path: '/users/:userId/challenges/:id',
            name: 'user.challenges.show',
            component: ChallengesShow,
            meta: {
                title: "Edit User",
                description: ""
            }
        },
        {
            path: '/users/:id/:tab',
            name: 'users.show',
            component: UserShow,
            meta: {
                title: "User Details",
                description: ""
            }
        },
        {
            path: '/challenges',
            name: 'challenges.index',
            component: ChallengesIndex,
            meta: {
                title: "Challenges List",
                description: ""
            }
        },
        {
            path: '/challenges/create',
            name: 'challenges.create',
            component: ChallengesCreate,
            meta: {
                title: "Add Challenge",
                description: ""
            }
        },
        {
            path: '/challenges/fit-to-path',
            name: 'fit_to_path.create',
            component: FitToPathCreate,
            meta: {
                title: "Fit To Path Points",
                description: ""
            }
        },
        {
            path: '/challenges/:id',
            name: 'challenges.show',
            component: ChallengesShow,
            meta: {
                title: "Challenge Details",
                description: ""
            }
        },
        {
            path: '/challenges/:id/edit',
            name: 'challenges.edit',
            component: ChallengesCreate,
            meta: {
                title: "Edit Challenge",
                description: ""
            }
        },
        {
            path: '/challenges/:id/exercises/:exerciseId/edit',
            name: 'challenges.exercise.edit',
            component: ChallengesCreate,
            meta: {
                title: "Edit Challenge",
                description: ""
            }
        },
        {
            path: '/feedback',
            name: 'feedback.index',
            component: FeedbackIndex,
            meta: {
                title: "Edit Challenge",
                description: ""
            }
        },
        {
            path: '/pooling-requests',
            name: 'pooling.requests.index',
            component: PoolingRequestsIndex,
            meta: {
                title: "Pooling Requests",
                description: ""
            }
        },
        {
            path: '/pooling-requests/:id',
            name: 'pooling.requests.show',
            component: PoolingRequestsShow,
            meta: {
                title: "Pooling Requests Details",
                description: ""
            }
        },
        {
            path: '/challenges/trivia/index',
            name: 'trivia.challenges.index',
            component: TriviaChallengeIndex,
            meta: {
                title: "Pooling Requests Details",
                description: ""
            }
        },
        // {
        //     path: '/charts',
        //     name: 'charts.index',
        //     component: eChartsComponent,
        //     meta: {
        //         title: "eCharts - Demo",
        //         description: ""
        //     }
        // },
    ],
});

router.beforeEach((to, from, next) => {
    // This goes through the matched routes from last to first, finding the closest route with a title.
    // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    // Find the nearest route element with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

    // If a route with a title was found, set the document (page) title to that value.
    if(nearestWithTitle) document.title = "Go Hard Finish Strong | " + nearestWithTitle.meta.title;

    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

    // Skip rendering meta tags if there are none.
    if(!nearestWithMeta) return next();

    // Turn the meta tag definitions into actual elements in the head.
    nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });

        // We use this to track which meta tags we create, so we don't interfere with other ones.
        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
    })
    // Add the meta tags to the document head.
        .forEach(tag => document.head.appendChild(tag));

    next();
});

export default router;
