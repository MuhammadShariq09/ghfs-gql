require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueGooglePlaces from 'vue-google-places';
import VuejsDialog from 'vuejs-dialog';

import VeeValidate from 'vee-validate';
import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css';

// import Echo from 'laravel-echo'

// import ECharts from 'vue-echarts' // refers to components/ECharts.vue in webpack


import BlockUI from 'vue-blockui';

Vue.use(BlockUI);
Vue.use(VuejsDialog, {
    html: true,
    loader: true,
    okText: 'Proceed',
    cancelText: 'Cancel',
    animation: 'bounce'
});
// include the default style
import 'vuejs-dialog/dist/vuejs-dialog.min.css';

window.toastr = require('toastr');

Vue.use(VueToastr2);
Vue.use(VeeValidate);
Vue.use(VueGooglePlaces);
Vue.use(VueRouter);

Vue.mixin({
    methods: {
        profileImage: (path, name, justReturn = false) => {
            if(justReturn)
                return path;
            return path? path:`https://ui-avatars.com/api/?rounded=true&background=20aaea&color=fff&name=${name}`
        }
    }
});


import router from './router';

Vue.component('top-header', require('./compoments/HeaderComponent').default);
Vue.component('side-navbar', require('./compoments/SidebarComponent').default);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('table-length', require('../components/TableLengthComponent').default);
// Vue.component('v-chart', ECharts);

import App from './compoments/AppComponent.vue';

const app = new Vue({
    el: '#app',
    router,
    components: { App },
    mounted(){

    }
});


$(document).on('keypress', 'input[type=number]', function(evt){
    if (evt.which !== 8 && evt.which !== 0 && evt.which < 48 || evt.which > 57)
        evt.preventDefault();
});

// window.Echo = new Echo({
//     broadcaster: 'socket.io',
//     host: window.location.hostname + ':1010'
// });
