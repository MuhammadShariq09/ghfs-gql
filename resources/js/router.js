import VueRouter from "vue-router";

import ExampleComponent from './components/ExampleComponent';

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: ExampleComponent
        },
        {
            path: '/example',
            name: 'example',
            component: ExampleComponent,
        },
    ],
});
