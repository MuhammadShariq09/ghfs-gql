const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

// mix.js('resources/js/app.js', 'public/js')
//     .sass([
//         "resources/app-assets/css/vendors.css",
//         "resources/app-assets/vendors/css/forms/icheck/icheck.css",
//         "resources/app-assets/css/app.css",
//         "resources/app-assets/vendors/css/forms/icheck/custom.css",
//         "resources/app-assets/css/core/menu/menu-types/vertical-menu.css",
//         "resources/app-assets/css/plugins/extensions/toastr.min.css",
//         "resources/app-assets/css/pages/login-register.css",
//         "resources/assets/css/style.css",
//         "resources/app-assets/vendors/css/tables/datatable/datatables.min.css",
//         "resources/app-assets/vendors/css/tables/datatable/datatables.min.css"
//     ], 'public/css/app.css');




mix.js([
    "resources/js/admin/app.js",
], 'public/admin-assets/js/app.js')
.sass('resources/sass/admin/app.scss', 'public/admin-assets/css').options({
    processCssUrls: false
});;
