<?php


function create($model, $attributes = [], $count = 1)
{
    return factory($model, $count)->create($attributes);
}

function make($model, $attributes = [], $count = 1)
{
    return factory($model, $count)->make($attributes);
}