<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_register()
    {
        Artisan::call('passport:install');

        $user = make('App\User', ['password' => 'arifiqbal']);


        $response = $this->post('/api/register', $user->toArray());

        $response->assertJsonStructure(['token']);
    }

    /** @test */
    public function a_user_can_login()
    {
        Artisan::call('passport:install');

        $user = create('App\User', [
            'email' => 'arifiqbal@outlook.com',
            'password' => bcrypt('arifiqbal')
        ]);

        $response = $this->post('/api/login', [
            'email' => 'arifiqbal@outlook.com',
            'password' => 'arifiqbal'
        ]);

        $response->assertJsonStructure(['token']);
    }
}
